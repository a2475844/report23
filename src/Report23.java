import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

public class Report23 {
    private JPanel root;
    private JButton button1Button;
    private JButton button2Button;
    private JButton button3Button;
    private JButton button4Button;
    private JButton button5Button;
    private JButton button6Button;
    private JButton checkOutButton;

    private JTextArea Food1;
    private JLabel Total;

    int sum = 0;


    public Report23() {

        button1Button.setIcon(new ImageIcon(
                this.getClass().getResource("Oyakodon.jpg")));
        button2Button.setIcon(new ImageIcon(
                this.getClass().getResource("Tendon.jpg")));
        button3Button.setIcon(new ImageIcon(
                this.getClass().getResource("Gyudon.jpg")));
        button4Button.setIcon(new ImageIcon(
                this.getClass().getResource("Kaisendon.jpg")));
        button5Button.setIcon(new ImageIcon(
                this.getClass().getResource("Katudon.jpg")));
        button6Button.setIcon(new ImageIcon(
                this.getClass().getResource("Kamamesidon.jpg")));
        Total.setText("Total "+sum+" yen");
        button1Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Oyakodon",700);

            }
        });
        button2Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tendon",850);

            }
        });
        button3Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyudon",400);


            }
        });
        button4Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Kaisendon",1100);

            }
        });
        button5Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Katudon",800);

            }
        });
        button6Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Kamamesidon",950);

            }
        });


        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(sum == 0){
                    JOptionPane.showMessageDialog(null,"You haven't chosen a product yet !" );
                }

                else{
                    Object[] option1 = {"cash", "card", "nanaco", "paypay"};
                    int confirmation = JOptionPane.showConfirmDialog(null,
                            "Would you like to check out ?",
                            "Checkout Confirmation",
                            JOptionPane.YES_NO_OPTION);

                    while (true) {

                        if(confirmation==1)
                            break;

                        if(confirmation==0) {
                            int  methods=JOptionPane.showOptionDialog(null, "Total price is "+sum+" yen",
                                    "Payment Methods",
                                    JOptionPane.DEFAULT_OPTION,
                                    JOptionPane.QUESTION_MESSAGE,
                                    null,option1,option1[0]);

                            if (methods == 0) {
                                JOptionPane.showMessageDialog(null, "Thank you !");
                                reset(0,"");
                                break;
                            }

                            if (methods == 1) {
                                String userInput = JOptionPane.showInputDialog(null,
                                        "Please type your card number",
                                        "Input",
                                        JOptionPane.INFORMATION_MESSAGE);

                                if (userInput !=null){
                                    JOptionPane.showMessageDialog(null,"Thank you." );
                                    reset(0,"");
                                    break;
                                }

                            }

                            if (methods == 2){
                                int confirmation2 = JOptionPane.showConfirmDialog(null,
                                        "Is it really a  nanaco card ?",
                                        "Checkout Confirmation",
                                        JOptionPane.YES_NO_OPTION);

                                if(confirmation2 == 0){
                                    Random rate = new Random();
                                    int ratePoint = rate.nextInt(6);
                                    JOptionPane.showMessageDialog(null,"Thank you for your payment !" );

                                    int getPoint = (sum/100)*ratePoint;
                                    JOptionPane.showMessageDialog(null, "You get " +getPoint+ " nanaco point !");
                                    reset(0,"");
                                    break;


                                }

                            }

                            if (methods == 3){
                                int confirmation3 = JOptionPane.showConfirmDialog(null,
                                        "This is only paypay. Are you okay ?",
                                        "Methods Confirmation",
                                        JOptionPane.YES_NO_OPTION);

                                if(confirmation3 == 0){
                                    ImageIcon qrIcon = new ImageIcon(this.getClass().getResource("qrcode.jpg"));
                                    JOptionPane.showMessageDialog(null,
                                            "Please load this QR code",
                                            "QR code",
                                            JOptionPane.INFORMATION_MESSAGE,
                                            qrIcon);
                                    JOptionPane.showMessageDialog(null, "Paypay !");
                                    reset(0,"");
                                    break;

                                }
                            }

                        }

                    }

                }


            }
        });
    }

    void order(String food ,int price){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order "+food+" ?\nThis is "+price+" yen",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if(confirmation==0) {
            Object[] Size = {"small (-50yen)", "normal", "big (+100yen)"};
            int  size =JOptionPane.showOptionDialog(null, "How big is it?",
                    "Size Confirmation",
                    JOptionPane.DEFAULT_OPTION,
                    JOptionPane.QUESTION_MESSAGE,
                    null,Size,Size[1]);
            if(size==0)
                price-=50;
            if(size==2)
                price+=100;
            JOptionPane.showMessageDialog(null, "Thank you for ordering " + food + " !");
            String currentText = Food1.getText();
            Food1.setText(currentText+food+" "+price+" yen\n");
            sum+=price;
            Total.setText("Total "+sum+" yen");
        }
    }

    void reset(int resetPrice,String resetText){
        sum=resetPrice;
        Total.setText("Total "+sum+" yen");
        Food1.setText(resetText);

    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Report23");
        frame.setContentPane(new Report23().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
